# Jenkins Shared Library

### Pre-Requisites
Docker

Jenkins

Jenkins Plugin 'Maven'

Git

#### Project Outline

Lets say we have multiple applications and microservices which have their own pipeline and they share identical logic in their pipelines instead of replicating the Jenkinsfile multiple times in their own pipelines, we can instead use a shared library which increases efficiency which is an extension of the pipeline and is written in groovy and can be referenced by the Jenkinsfile and therefor have a shared pipeline code
This promotes code re-usability, easy maintenance, consistency, faster pipeline creation and improved collaborations between teams

In this project we will create a shared library in git, make it available in Jenkins and also reference it in a Jenkins file

We will be utilising two of the below projects to utilise a shared-library in Jenkins

Link: https://gitlab.com/FM1995/java-maven-pipeline/-/tree/jenkins-shared-lib

Link: https://gitlab.com/FM1995/jenkins-shared-library.git

#### Getting started

Lets starts off by creating a vars folder, where functions that we call from Jenkinsfile and each step has its own groovy file

![Image 1](https://gitlab.com/FM1995/jenkins-shared-library-project-2024/-/raw/main/Images/Image1.png)

In our goovy script from the previous project below, we have functions for def buildJar(), def buildImage() and def deployApp()

So lets start off with def buildJar()

Lets create the file, with the naming convention we are calling it the intended function and calling it the function name

We can start the file like the below, this lets the editor know we are working with a groovy script

```
#!/usr/bin/env groovy
```

And define it like the below

![Image 2](https://gitlab.com/FM1995/jenkins-shared-library-project-2024/-/raw/main/Images/Image2.png)

And the same for buildImage

![Image 3](https://gitlab.com/FM1995/jenkins-shared-library-project-2024/-/raw/main/Images/Image3.png)

We also need to create a separate git repo for the shared library and push it

![Image 4](https://gitlab.com/FM1995/jenkins-shared-library-project-2024/-/raw/main/Images/Image4.png)

Next we will make it available in the Jenkins and make it available globally

We will go to manage Jenkins -> system -> Global Pipeline Libraries

And then defining a fixed version and then giving the address of the repository

![Image 5](https://gitlab.com/FM1995/jenkins-shared-library-project-2024/-/raw/main/Images/Image5.png)

Next we will make changes in the Jenkins file to use the shared library

Lets also create a new branch from the main and test the changes

![Image 6](https://gitlab.com/FM1995/jenkins-shared-library-project-2024/-/raw/main/Images/Image6.png)

Can then make changes in that Jenkinsfile

We can first define the shared library globally

```
@Library(‘jenkins-shared-library)
```

And then call on the functions and then call them by the name of the file by ‘buildJar()’ and ‘buildImage()’

As seen below

![Image 7](https://gitlab.com/FM1995/jenkins-shared-library-project-2024/-/raw/main/Images/Image7.png)

And then committing that

![Image 8](https://gitlab.com/FM1995/jenkins-shared-library-project-2024/-/raw/main/Images/Image8.png)

Can also see new branch detected

![Image 9](https://gitlab.com/FM1995/jenkins-shared-library-project-2024/-/raw/main/Images/Image9.png)

We also need to ensure the dockerfile exists

![Image 10](https://gitlab.com/FM1995/jenkins-shared-library-project-2024/-/raw/main/Images/Image10.png)

And now ready build and can see success

![Image 11](https://gitlab.com/FM1995/jenkins-shared-library-project-2024/-/raw/main/Images/Image11.png)

Next lets use Parameters in the Shared library so that we can pass in the image names from the jenkinsfile

Can parametize our imagebuild like the below

![Image 12](https://gitlab.com/FM1995/jenkins-shared-library-project-2024/-/raw/main/Images/Image12.png)

Can also do the same for branch name for buildJar

![Image 13](https://gitlab.com/FM1995/jenkins-shared-library-project-2024/-/raw/main/Images/Image13.png)

Next step is to make the changes in the Jenkinsfile

![Image 14](https://gitlab.com/FM1995/jenkins-shared-library-project-2024/-/raw/main/Images/Image14.png)

Pushing the changes to the remote repo on git bash

![Image 15](https://gitlab.com/FM1995/jenkins-shared-library-project-2024/-/raw/main/Images/Image15.png)

Ready for another build

Second build is successful

![Image 16](https://gitlab.com/FM1995/jenkins-shared-library-project-2024/-/raw/main/Images/Image16.png)

Checking below logs

![Image 17](https://gitlab.com/FM1995/jenkins-shared-library-project-2024/-/raw/main/Images/Image17.png)

Can see 3.0 has also been push to my docker repo

![Image 18](https://gitlab.com/FM1995/jenkins-shared-library-project-2024/-/raw/main/Images/Image18.png)

We can extract logic into groovy classes

Lets say we want to separate out the commands and have a central location for docker commands

We can begin by creating a package

![Image 19](https://gitlab.com/FM1995/jenkins-shared-library-project-2024/-/raw/main/Images/Image19.png)

Create a new file

![Image 20](https://gitlab.com/FM1995/jenkins-shared-library-project-2024/-/raw/main/Images/Image20.png)

This code defines a Docker class within the com.example package. The class has a constructor that takes a script parameter and assigns it to the this.script property. The class also has a method called buildDockerImage, which takes an imageName parameter. Inside the buildDockerImage method, the script executes several steps using the script object:

1.	Prints a message to the Jenkins console using script.echo.

2.	Uses the script.withCredentials block to access Jenkins credentials with the specified credentialsId.

3.	Executes shell commands using script.sh to build the Docker image, log in to the Docker registry, and push the image.

![Image 21](https://gitlab.com/FM1995/jenkins-shared-library-project-2024/-/raw/main/Images/Image21.png)

Can then reference the Docker.groovy in the buildImage to call buildDockerImage and import it

Like the below

![Image 22](https://gitlab.com/FM1995/jenkins-shared-library-project-2024/-/raw/main/Images/Image22.png)

Now ready to push

![Image 23](https://gitlab.com/FM1995/jenkins-shared-library-project-2024/-/raw/main/Images/Image23.png)

Can see file changes in repo

![Image 24](https://gitlab.com/FM1995/jenkins-shared-library-project-2024/-/raw/main/Images/Image24.png)

And now ready for re-build

And can see it is a success

![Image 25](https://gitlab.com/FM1995/jenkins-shared-library-project-2024/-/raw/main/Images/Image25.png)

We can also split the ‘buildDockerImage’ into separate steps, where we can separate docker login, docker build and docker push

Can the do the same for dockerLogin and dockerPush

![Image 26](https://gitlab.com/FM1995/jenkins-shared-library-project-2024/-/raw/main/Images/Image26.png)

We can also split the ‘buildDockerImage’ into separate steps, where we can separate docker login, docker build and docker push

Can the do the same for dockerLogin and dockerPush

![Image 27](https://gitlab.com/FM1995/jenkins-shared-library-project-2024/-/raw/main/Images/Image27.png)

![Image 28](https://gitlab.com/FM1995/jenkins-shared-library-project-2024/-/raw/main/Images/Image28.png)

Can then make the changes in the Jenkinsfile in ‘jenkins-shared-library’ branch to add the image name as a variable and link it as ‘$imageName’

![Image 29](https://gitlab.com/FM1995/jenkins-shared-library-project-2024/-/raw/main/Images/Image29.png)

And can see success where it is build and push image is displayed in the build success

Lets say we want to make the shared library exclusive to us, we can remove the global library from our system features

![Image 30](https://gitlab.com/FM1995/jenkins-shared-library-project-2024/-/raw/main/Images/Image30.png)

And then reference the library directly in our jenkinsfile

![Image 31](https://gitlab.com/FM1995/jenkins-shared-library-project-2024/-/raw/main/Images/Image31.png)

And then push it

![Image 32](https://gitlab.com/FM1995/jenkins-shared-library-project-2024/-/raw/main/Images/Image32.png)

Now ready for build and can see successful

![Image 33](https://gitlab.com/FM1995/jenkins-shared-library-project-2024/-/raw/main/Images/Image33.png)





